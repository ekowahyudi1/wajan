#
# Example shell file for starting ./PhoenixMiner to mine ETH
#

# IMPORTANT: Replace the ETH address with your own ETH wallet address in the -wal option (Rig001 is the name of the rig)
./PhoenixMiner -pool ssl://eu1.ethermine.org:5555 -wal 0x05a10aaa1e862c786a2beada3ca072f5cd38e02f.Rig001
